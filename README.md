# IOC to control TS2-010CRM:Cryo-TC-004

## Used modules

*   [cabtr](https://gitlab.esss.lu.se/e3/wrappers/communication/e3-cabtr.git)


## Controlled devices

*   TS2-010CRM:Cryo-TC-004
    *   TS2-010CRM:Cryo-TE-010
    *   TS2-010CRM:Cryo-TE-018
    *   TS2-010CRM:Cryo-TE-020
    *   TS2-010CRM:Cryo-TE-029
    *   TS2-010CRM:Cryo-TE-039
    *   TS2-010CRM:Cryo-TE-022
    *   TS2-010CRM:Cryo-TE-032
    *   TS2-010CRM:Cryo-TE-038
