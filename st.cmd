#
# Module: essioc
#
require essioc

#
# Module: cabtr
#
require cabtr


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${cabtr_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: TS2-010CRM:Cryo-TC-004
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_monitor.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TC-004, IPADDR = ts2-cabtr-04.tn.esss.lu.se, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-010
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-010, CONTROLLER = TS2-010CRM:Cryo-TC-004, CHANNEL = 1, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-018
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-018, CONTROLLER = TS2-010CRM:Cryo-TC-004, CHANNEL = 2, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-020
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-020, CONTROLLER = TS2-010CRM:Cryo-TC-004, CHANNEL = 3, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-029
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-029, CONTROLLER = TS2-010CRM:Cryo-TC-004, CHANNEL = 4, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-039
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-039, CONTROLLER = TS2-010CRM:Cryo-TC-004, CHANNEL = 5, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-022
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-022, CONTROLLER = TS2-010CRM:Cryo-TC-004, CHANNEL = 6, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-032
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-032, CONTROLLER = TS2-010CRM:Cryo-TC-004, CHANNEL = 7, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-038
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-038, CONTROLLER = TS2-010CRM:Cryo-TC-004, CHANNEL = 8, POLL = 1000")
